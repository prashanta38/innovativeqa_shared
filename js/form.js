function clearTestProcess() {
	document.getElementById('cbAutomationId').checked = false;
	document.getElementById('cbManualId').checked = false;
}

function clearTestCaseType (){
	document.getElementById('cbFunctionaManualId').checked = false;
	document.getElementById('cbTestFunctionalAutomatedId').checked = false;
	document.getElementById('cbAccessibilityId').checked = false;

	document.getElementById('cbFunctionaManualId').disabled = false;
	document.getElementById('cbTestFunctionalAutomatedId').disabled = false;
	document.getElementById('cbAccessibilityId').disabled = false;

	if (document.getElementById('cbPerformanceId')) {
		document.getElementById('cbPerformanceId').checked = false;
		document.getElementById('cbPerformanceId').disabled = false;
	}
}

//==========================================================================
function radioServAppTestPlan() {
	document.getElementById('testProcessDiv').style.display = 'none';
	document.getElementById('testCaseType').style.display = 'none';
}


function radioServAppTestCases() {
	document.getElementById('testProcessDiv').style.display = 'none';
	document.getElementById('testCaseType').style.display = 'block';
	clearTestCaseType();
	clearTestProcess();
}

function radioServAppFullTest() {
	document.getElementById('testProcessDiv').style.display = 'block';
	document.getElementById('testCaseType').style.display = 'none';
	clearTestProcess();
	clearTestCaseType();
}

//==============================================================================
function cbProcessAutomation() {
	document.getElementById('testCaseType').style.display = 'block';
	if (document.getElementById('cbAutomationId').checked == true ) {
		document.getElementById('cbTestFunctionalAutomatedId').checked = true;
		document.getElementById('cbTestFunctionalAutomatedId').disabled = false;
		document.getElementById('cbFunctionaManualId').disabled = true;
		if (document.getElementById('cbManualId').checked == true ) {
			document.getElementById('cbFunctionaManualId').checked = true;
			document.getElementById('cbFunctionaManualId').disabled = false;
		}
	} else {
		if (document.getElementById('cbManualId').checked == false )
			document.getElementById('testCaseType').style.display = 'none';
		document.getElementById('cbTestFunctionalAutomatedId').checked = false;
		document.getElementById('cbTestFunctionalAutomatedId').disabled = true;
	}
}

function cbProcessManual() {
	document.getElementById('testCaseType').style.display = 'block';
	if (document.getElementById('cbManualId').checked == true ) {
		document.getElementById('cbFunctionaManualId').checked = true;
		document.getElementById('cbFunctionaManualId').disabled = false;
		document.getElementById('cbTestFunctionalAutomatedId').disabled = true;
		if (document.getElementById('cbAutomationId').checked == true ) {
			document.getElementById('cbTestFunctionalAutomatedId').checked = true;
			document.getElementById('cbTestFunctionalAutomatedId').disabled = false;
		}
	} else {
		if (document.getElementById('cbAutomationId').checked == false )
			document.getElementById('testCaseType').style.display = 'none';
		document.getElementById('cbFunctionaManualId').checked = false;
		document.getElementById('cbFunctionaManualId').disabled = true;
	}
}

function cbProcessBoth() {
	document.getElementById('testCaseType').style.display = 'block';
	if (document.getElementById('cbProcessBothId').checked == true ) {
		document.getElementById('cbAutomationId').checked = false;
		document.getElementById('cbManualId').checked = false;
		document.getElementById('cbFunctionaManualId').checked = true;
		document.getElementById('cbTestFunctionalAutomatedId').checked = true;
	} else {
		document.getElementById('cbFunctionaManualId').checked = false;
		document.getElementById('cbTestFunctionalAutomatedId').checked = false;
	}
}

//=============================================================================
function cbAndroidClick() {
	document.getElementById('cbOsTypeBothId').checked = false;
}

function cbIosClick() {
	document.getElementById('cbOsTypeBothId').checked = false;
}

function cbOsTypeBothClick() {
	if (document.getElementById('cbOsTypeBothId').checked == true) {
		document.getElementById('cbAndroidId').checked = false;
		document.getElementById('cdIosId').checked = false;
	}
}
